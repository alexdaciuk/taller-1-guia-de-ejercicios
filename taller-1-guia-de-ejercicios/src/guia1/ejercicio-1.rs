use rand::Rng;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn main() {
  let mut rng = rand::thread_rng();

  let words = read_file("../files/words.txt");
  println!("Words len is {}", words.len());
  let word = String::from(&words[rng.gen_range(0..words.len())]);

  let mut juego = Ahorcado {
    vidas: word.chars().count(),
    letras_usadas: Vec::new(),
    letras_acertadas: Vec::new(),
    aciertos: vec!['-'; word.chars().count()],
    palabra: word,
  };

  println!("Bienvenido al ahorcado de FIUBA!");

  while juego.vidas > 0 && juego.letras_acertadas.len() <= juego.palabra.len() {
    print_state(&juego);

    let mut i = String::new();
    print!("Por favor ingrese una letra : ");
    io::stdin()
      .read_line(&mut i)
      .expect("Error leyendo la linea.");

    let v = i.chars().next().expect("string is empty");

    if juego.palabra.contains(&i.trim()) {
      juego.letras_acertadas.push(v);
      juego.letras_usadas.push(v);
      juego.aciertos[juego.palabra.rfind(v).unwrap_or(0)] = v;
    } else {
      juego.vidas -= 1;
    }
  }
}

fn print_state(juego: &Ahorcado) {
  println!("La palabra hasta el momento es : {:?} ", juego.aciertos);
  println!(
    "Adivinaste las siguientes letras : {:?}",
    juego.letras_acertadas
  );
  println!("Te quedan {} intentos", juego.vidas);
}

struct Ahorcado {
  aciertos: Vec<char>,
  vidas: usize,
  letras_acertadas: Vec<char>,
  letras_usadas: Vec<char>,
  palabra: String,
}

fn read_file(path: &str) -> Vec<String> {
  let mut words = Vec::new();

  if let Ok(lines) = read_lines(path) {
    for line in lines.into_iter().flatten() {
      words.push(line);
    }
  }

  words
}

// The output is wrapped in a Result to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
  P: AsRef<Path>,
{
  let file = File::open(filename)?;
  Ok(io::BufReader::new(file).lines())
}
