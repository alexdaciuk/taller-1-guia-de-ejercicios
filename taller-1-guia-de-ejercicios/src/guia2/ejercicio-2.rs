use std::cmp;
use std::env;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn main() {
  let args: Vec<_> = env::args().collect();

  if args.len() != 3 {
    panic!("Need 2 files to compare!")
  }

  let file_a_lines = read_files_lines(&args[1]);
  let file_b_lines = read_files_lines(&args[2]);

  // Debug
  println!("File a contains : {:?}", file_a_lines);
  println!("File b contains : {:?}", file_b_lines);

  // Call LCS
  let grid = get_lcs(&file_a_lines, &file_b_lines);

  println!("{:?}", grid);

  print_diff(
    grid,
    &file_a_lines,
    &file_b_lines,
    file_a_lines.len(),
    file_b_lines.len(),
  );
}

// LCS
fn get_lcs(lines_a: &[String], lines_b: &[String]) -> Vec<Vec<u8>> {
  let m = lines_a.len();
  let n = lines_b.len();

  let mut grid: Vec<Vec<u8>> = vec![vec![0; n + 1]; m + 1];

  for i in 0..m {
    for j in 0..n {
      if lines_a[i] == lines_b[j] {
        grid[i + 1][j + 1] = grid[i][j] + 1;
      } else {
        grid[i + 1][j + 1] = cmp::max(grid[i + 1][j], grid[i][j + 1]);
      }
    }
  }

  grid
}

// Print diff
fn print_diff(grid: Vec<Vec<u8>>, lines_a: &[String], lines_b: &[String], i: usize, j: usize) {
  if i > 0 && j > 0 && lines_a[i - 1] == lines_b[j - 1] {
    print_diff(grid, lines_a, lines_b, i - 1, j - 1);
    println!(" {}", lines_a[i - 1]);
  } else if j > 0 && (i == 0 || grid[i][j - 1] >= grid[i - 1][j]) {
    print_diff(grid, lines_a, lines_b, i, j - 1);
    println!("> {}", lines_b[j - 1]);
  } else if i > 0 && (j == 0 || grid[i][j - 1] < grid[i - 1][j]) {
    print_diff(grid, lines_a, lines_b, i - 1, j);
    println!("< {}", lines_a[i - 1]);
  } else {
    println!(" ");
  }
}

// Get lines into a vector
fn read_files_lines(filename: &str) -> Vec<String> {
  let mut vec = Vec::new();

  if let Ok(lines) = read_lines(filename) {
    for line in lines.into_iter().flatten() {
      vec.push(line);
    }
  }

  vec
}

// The output is wrapped in a Result to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
  P: AsRef<Path>,
{
  let file = File::open(filename)?;
  Ok(io::BufReader::new(file).lines())
}
